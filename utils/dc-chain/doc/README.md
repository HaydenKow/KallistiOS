# Sega Dreamcast Toolchain Maker (`dc-chain`) Documentation #

This directory contains helpful information and/or files in order to build a
working environment for **Sega Dreamcast** programming, depending of your host
environment.

Tested environments are:

- **BSD** (`FreeBSD 11.2`)
- **Cygwin**
- **GNU/Linux** (`Lubuntu 18.04`)
- **macOS** (`macOS Mojave 10.14`)
- **MinGW/MSYS**
- **MinGW-w64/MSYS2**

