$(info dc-chain Makefile)

# Check the presence of ./config.guess
# This will help a lot to execute conditional steps depending on the host.
config_guess_check=$(shell test -f ./config.guess || echo 1)
ifeq ($(config_guess_check),1)
  $(error Please execute ./download.sh first)
endif

# Retrieve the host triplet
host_triplet=$(shell ./config.guess)

# Retrieve the system
uname_s := $(shell uname -s)

# Determine if we are running macOS
ifeq ($(uname_s),Darwin)
  $(info macOS build environment detected)
  MACOS := 1
endif

# Determine if we are running Linux
ifeq ($(uname_s),Linux)
  $(info GNU/Linux build environment detected)
  LINUX := 1
endif

# Determine if we are running under FreeBSD
ifeq ($(shell echo $(host_triplet) | grep -q "freebsd" && echo "1"),1)
  $(info FreeBSD build environment detected)
  FREEBSD := 1
endif

# Determine if we are running Cygwin
ifneq ($(shell echo $(host_triplet) | grep -i 'cygwin'),)
  CYGWIN := 1
  WINDOWS := 1  
endif

# Determine if we are running under MinGW/MSYS
# This will make difference between MinGW/MSYS and MinGW-w64/MSYS2
ifneq ($(shell echo $(host_triplet) | grep -i 'mingw'),)
  is_legacy_mingw=$(shell pacman --version >/dev/null 2>&1 || echo 1)
  ifeq ($(is_legacy_mingw),1)
    $(info MinGW/MSYS build environment detected)
    MINGW := 1
  else
    $(info MinGW-w64/MSYS2 build environment detected)
    MINGW64 := 1
# config.guess is not reliable in this environment	
    host_triplet=$(shell echo $$MSYSTEM_CHOST)
  endif
  MINGW32 := 1  
  WINDOWS := 1  
endif

# Check the MSYS POSIX emulation layer version...
#
# This is needed as a severe bug exists in the /bin/msys-1.0.dll file.
# Indeed, the heap size is too small and hardcoded, which causes the error:
#   Couldn't commit memory for cygwin heap, Win32 error
#
# The only solution to build gcc for sh-elf target under MinGW/MSYS is to
# temporarily replace the /bin/msys-1.0.dll shipped with MinGW environment
# (which is the v1.0.19 at this time) with the patched version provided in the
# ./doc/mingw/ directory. You just need the msys-1.0.dll file of this
# package. Copy the original file somewhere and replace it with the provided
# patched version.
#
# The patched version is coming from the C::B Advanced package. The major fix 
# applied is the heap size increase, from 256 MB to more than 1024 MB.
#
# After building the whole toolchain, please remove the patched version and move
# the original /bin/msys-1.0.dll file back in place.
ifdef MINGW
  msys_patched_checksum=2e627b60938fb8894b3536fc8fe0587a5477f570
  msys_checksum=$(shell sha1sum /bin/msys-1.0.dll | cut -c-40)
  ifneq ($(msys_checksum),$(msys_patched_checksum))	
    $(warning Please consider temporarily patching /bin/msys-1.0.dll!)
  endif
endif
